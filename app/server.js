import { dirname } from 'path';
import { fileURLToPath } from 'url';
import http from 'http';
import ws from 'websocket';
import express from 'express';
import { v4 as uuidv4 } from 'uuid';

// TODO: single app state
// TODO: move method names, hashes, servers
const appClients = {};
const appGames = {};

const __dirname = dirname(fileURLToPath(import.meta.url));
const httpServer = http.createServer();
const WebSocketServer = ws.server;
const websocketServer = new WebSocketServer({
  httpServer: httpServer,
});
const app = express();
const colorTypes = {
  0: 'Red',
  1: 'Green',
  2: 'Blue',
};

app.get('/', (req, res) => res.sendFile(`${__dirname}/index.html`));
app.listen(8080, () => console.log('Listening on port 8080...'));

const updateGameState = () => {
  Object.values(appGames).forEach((g) => {
    g.clients.forEach(({ clientId }) => {
      const clientConnection = appClients[clientId].connection;
      const payload = { method: 'update', state: g.state };
      clientConnection.send(JSON.stringify(payload));
    });
  });

  setTimeout(updateGameState, 500);
};

websocketServer.on('request', (request) => {
  // Accept any protocol connection
  const connection = request.accept(null, request.origin);

  // Assign new client an ID
  // Save client's connection in app state
  // Notify client with new connection
  const clientId = uuidv4();
  const payload = { method: 'connect', clientId };

  appClients[clientId] = { connection };
  connection.send(JSON.stringify(payload));

  // Handle WS events
  connection.on('open', () => console.log('SERVER: connection opened'));
  connection.on('close', () => console.log('SERVER: connection closed'));
  connection.on('message', (message) => {
    const data = JSON.parse(message.utf8Data);
    console.log('SERVER received message:', data);

    // Create new game
    // Save game in app state
    // Notify client with new game
    if (data.method === 'create') {
      const clientId = data.clientId;
      const clientConnection = appClients[clientId].connection;
      const gameId = uuidv4();
      const game = { id: gameId, cells: 20, clients: [] };
      const payload = { method: 'create', game };

      appGames[gameId] = game;
      clientConnection.send(JSON.stringify(payload));
    }

    // Join game
    // Assign color
    // Add client to app game state
    // Notify clients with new member
    if (data.method === 'join') {
      const { clientId, gameId } = data;
      const game = appGames[gameId];

      if (!game) {
        console.log('No game available:', gameId);
      } else if (!game.clients) {
        console.log('No game clients available:', game.clients);
      } else if (game.clients.find((c) => c.clientId === clientId)) {
        console.group('Player already added to the game:');
        console.log(`Game ID: ${gameId}`);
        console.log(`Client ID: ${clientId}`);
        console.groupEnd();
      } else if (game.clients.length >= 3) {
        console.log('Max players reached:', game.clients.length);
      } else {
        const clientCount = game.clients.length;
        const color = colorTypes[clientCount];
        const payload = { method: 'join', game };

        game.clients.push({ clientId, color });
        game.clients.forEach(({ clientId }) => {
          const clientConnection = appClients[clientId].connection;
          clientConnection.send(JSON.stringify(payload));
        });

        // Start the game
        if (game.clients.length === 3) {
          updateGameState();
        }
      }
    }

    // Play action
    // Update state with clicked cell ID & client color
    if (data.method === 'play') {
      const { gameId, cellId, clientColor } = data;

      appGames[gameId].state = {
        ...appGames[gameId].state,
        [cellId]: clientColor,
      };
    }
  });
});

httpServer.listen(8081, () => console.log('Listening on port 8081...'));
